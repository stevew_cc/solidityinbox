// contract test code will go here
const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());
const { abi, evm } = require('../build/Lottery');

let lottery;
let accounts;

beforeEach(async() =>{
    // get a list of all accounts
    accounts = await web3.eth.getAccounts();

    // use one of those accounts to deploy the contract
    lottery = await new web3.eth.Contract(abi)
        .deploy({
            data: evm.bytecode.object,
        })
        .send({
            from: accounts[0],
            gas: '1000000'
        })
});

describe('Lottery Contract', () => {
    it('should  deploy a contract', function () {
        assert.ok(lottery.options.address);
    });

    it('should allow one account to enter', async () => {
        await lottery.methods.enter().send({
            from: accounts[0],
            value: web3.utils.toWei('0.02', "ether")
        });

        const players = await lottery.methods.getPlayers().call({
            from: accounts[0]
        });

        assert.equal(accounts[0], players[0]);
        assert.equal(1, players.length);

    });

    it('should allow multiple to enter', async () => {
        await lottery.methods.enter().send({
            from: accounts[0],
            value: web3.utils.toWei('0.02', "ether")
        });
        await lottery.methods.enter().send({
            from: accounts[1],
            value: web3.utils.toWei('0.02', "ether")
        });
        await lottery.methods.enter().send({
            from: accounts[2],
            value: web3.utils.toWei('0.02', "ether")
        });

        const players = await lottery.methods.getPlayers().call({
            from: accounts[0]
        });

        assert.equal(accounts[0], players[0]);
        assert.equal(accounts[1], players[1]);
        assert.equal(accounts[2], players[2]);
        assert.equal(3, players.length);
    });

    it('requires a minimum amount of ether to enter', async () => {
        let executed;
        try {
            await lottery.methods.enter().send({
                from: accounts[0],
                value: '100'
            });
            executed = 'success';
        } catch (err) {
            executed = 'fail'
        }

        assert.equal('fail', executed);
    });

    it('only manager should pick a winner', async () => {
        try{
            await lottery.methods.enter().send({
                from: accounts[0],
                value: web3.utils.toWei('0.02', "ether")
            });

            await lottery.methods.pickWinner().send({
                from: accounts[1]
            });
            assert(false);

        } catch(e){

           (!(e instanceof assert.AssertionError)) ?
                console.log("Non Manager has picked winner") :
               console.log("Manager has picked winner");

            assert(!(e instanceof assert.AssertionError));
        }



    });

    it('should send money to the winner and clear players', async () => {
        const startBalance = await web3.eth.getBalance(accounts[0]);
        //console.log(web3.utils.fromWei(startBalance, "ether"));

        await lottery.methods.enter().send({
            from: accounts[0],
            value: web3.utils.toWei('2', "ether")
        });

        const currBalance = await web3.eth.getBalance(lottery.options.address);
        assert.equal(currBalance, web3.utils.toWei('2', "ether"));

        const intBalance = await web3.eth.getBalance(accounts[0]);

        await lottery.methods.pickWinner().send({
            from: accounts[0]
        });

        const finalBalance = await web3.eth.getBalance(accounts[0]);

        const diff = (finalBalance - intBalance);

        assert(diff > web3.utils.toWei('1.8', "ether"));
    });


})
