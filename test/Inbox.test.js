// contract test code will go here
const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const { abi, evm } = require('../build/Inbox');
const INITIAL_MESSAGE = 'is set'
const SET_MESSAGE = 'new'

const web3 = new Web3(ganache.provider());

let accounts;
let inbox;

beforeEach(async() =>{
    // get a list of all accounts
    accounts = await web3.eth.getAccounts();

    // use one of those accounts to deploy the contract
    inbox = await new web3.eth.Contract(abi)
        .deploy({
            data: evm.bytecode.object,
            arguments: [INITIAL_MESSAGE]
        })
        .send({
            from: accounts[0],
            gas: '1000000'
        })

});

describe('Inbox',  ()=>{
    it('Deploys a contract ',()=>{
        assert.ok(inbox.options.address);
    });

    it('Has an initial message',async ()=>{
        const message = await inbox.methods.message().call();
        assert.equal(message, INITIAL_MESSAGE);
    });

    it('can set the message',async ()=>{
       await inbox.methods.setMessage(SET_MESSAGE).send({from: accounts[0], gas: '1000000'});
       const newMessage = await inbox.methods.message().call();
       assert.equal(newMessage, SET_MESSAGE);
    });
});

