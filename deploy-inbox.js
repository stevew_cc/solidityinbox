// deploy code will go here
const HDWalletProvider = require('@truffle/hdwallet-provider');
const Web3 = require('web3');
const { abi, evm } = require('./compile');

const INITIAL_MESSAGE = 'is set'

const provider = new HDWalletProvider(
    'they pig vintage frozen foot cliff runway input fly decade biology economy',
    'wss://rinkeby.infura.io/ws/v3/15b2725e3d0343fe9cde0b730690da04'
)

const web3 = new Web3(provider);

const deployInbox = async () => {
    // get a list of all accounts
    // Account 1 0x2661Da38e99465b4688779249cc0dF1b606E109e
    const accounts = await web3.eth.getAccounts();
    console.log(accounts[0]);
    //use one of those accounts to deploy the contract
    const result = await new web3.eth.Contract(abi)
        .deploy({data: evm.bytecode, arguments: [INITIAL_MESSAGE]})
        .send({from: accounts[0], gas: '1000000'});

    console.log('Contract deployed to', result.options.address);
    provider.engine.stop();
    // 0xeF25118261150F691780F84A29E4B8344561f9a9
};

deployInbox();


