import web3 from './web3';
import lotteryContract from './build/Lottery.json';

//const address = '0xF4Ff3fa61d0846ebeD10892E3FFaF7184a475BB2'; // RinkeBy
const address = '0x021381495774Ed1571060459343Ff8684c6c62E0'; // Ropsten

const loadData = JSON.parse(JSON.stringify(lotteryContract));

export default new web3.eth.Contract(loadData.abi, address);
