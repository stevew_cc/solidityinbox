import { useState,useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import web3 from './web3';
import lottery from './lottery';


const App =  () =>  {
  const [accounts, setAccounts] = useState('');
  const [manager, setManager] = useState('');
  const [players, setPlayers] = useState([]);
  const [balance, setBalance] = useState('');
  const [value, setValue] = useState('');
  const [message, setMessage] = useState('');

  useEffect(() => {
    if (!accounts) {
      getAccounts();
    }
    if (!manager) {
      getManager();
    }

    getPlayers();
    getBalance();

  }, [accounts, manager, players, balance]);

  const getAccounts = async () => {
    setAccounts( await web3.eth.getAccounts() );
  }

  const getManager = async () => {
    setManager(await lottery.methods.manager().call());
  }

  const getPlayers = async () => {
    setPlayers(await lottery.methods.getPlayers().call());
  }

  const getBalance = async () => {
    setBalance(await web3.eth.getBalance(lottery.options.address));
  }

  const onSubmit = async (event) => {
    event.preventDefault();

    console.log(accounts[0]);

    setMessage('Awaiting for transaction success...')

    await lottery.methods.enter().send({
      from: accounts[0],
      value: web3.utils.toWei(value, 'ether')
      //value: 12000000000000000
    });

    setMessage('You have been entered!')
  }

  const onClick = async (event) => {
    getAccounts();
    console.log(accounts);

    setMessage('Awaiting for lottery result...');

    await lottery.methods.pickWinner().send({
      from: accounts[0]
    })

    setMessage('Winner Picked!')
  }

  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Lottery Contract</h2>
          <p>
          {  !manager && <>Manager is loading<br/></> }
            {  manager && <>This contract is managed by { manager }<br/></> }
          {  players && <>Num of player entered { players.length }<br/></> }
          {  balance && <>The contract balance is { web3.utils.fromWei(balance, 'ether') }<br/></> }
          </p>
          <form action="" onSubmit={onSubmit}>
            <h2>Enter the lottery</h2>
            <fieldset>
              <label htmlFor="eth">Amount to enter in Ether</label>
              <br/>
              <input
                  value={value}
                  type="text"
                  name="eth"
                  onChange={ event => setValue(event.target.value) }
              />
              <button>Enter contest</button>

              { message && <><hr/><h2>{message}</h2></> }

            </fieldset>

          </form>
        </header>
        <section >
          <h2>Ready to Pick a Winner?</h2>
          <button className="btn primary-btn" onClick={onClick}>Pick a winner</button>
        </section>
        <hr/>
      </div>
  );
}

export default App;
